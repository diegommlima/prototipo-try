//
//  PQViewController.m
//  PrototypeAnimation
//
//  Created by Diego Lima on 23/09/14.
//  Copyright (c) 2014 Diego Lima. All rights reserved.
//

#import "PQAlertViewController.h"
#import "AMSmoothAlertView.h"
#import "AMSmoothAlertConstants.h"

@interface PQAlertViewController () <AMSmoothAlertViewDelegate>

@property (nonatomic, strong) AMSmoothAlertView * alert;

@end

@implementation PQAlertViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.title = @"Alertas";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)accessSuccess:(id)sender {
    
    [self openAlertCustomWithType:AlertSuccess andTitle:@"Parabéns!" andButtonName:@"Ok"];
    
}

- (IBAction)accessFail:(id)sender {
    
    [self openAlertCustomWithType:AlertFailure andTitle:@"Desculpe!" andButtonName:@"Ok"];

}

- (IBAction)accessInfo:(id)sender {
    
    [self openAlertCustomWithType:AlertInfo andTitle:@"Atenção!" andButtonName:@"Ok"];

}

- (void)openAlertCustomWithType:(AlertType)type andTitle:(NSString *)title andButtonName:(NSString *)titleButton {

    if (!self.alert || !self.alert.isDisplayed) {
        self.alert = [[AMSmoothAlertView alloc]initDropAlertWithTitle:title andText:@"Ai está o alertView Customizado!" andCancelButton:NO forAlertType:type];
        [self.alert.defaultButton setTitle:titleButton forState:UIControlStateNormal];
        self.alert.completionBlock = ^void (AMSmoothAlertView *alertObj, UIButton *button) {
            if(button == alertObj.defaultButton) {
                NSLog(@"Default");
            } else {
                NSLog(@"Others");
            }
        };
        
        self.alert.cornerRadius = 3.0f;
        [self.alert show];
    }
    else {
        
        [self.alert dismissAlertView];

    }
}

#pragma mark - Delegates
- (void)alertView:(AMSmoothAlertView *)alertView didDismissWithButton:(UIButton *)button {
	if (alertView == self.alert) {
		if (button == self.alert.defaultButton) {
			NSLog(@"Default button touched!");
		}
		if (button == self.alert.cancelButton) {
			NSLog(@"Cancel button touched!");
		}
	}
}

- (void)alertViewWillShow:(AMSmoothAlertView *)alertView {
    if (alertView.tag == 0)
        NSLog(@"AlertView Will Show: '%@'", alertView.titleLabel.text);
}

- (void)alertViewDidShow:(AMSmoothAlertView *)alertView {
	NSLog(@"AlertView Did Show: '%@'", alertView.titleLabel.text);
}

- (void)alertViewWillDismiss:(AMSmoothAlertView *)alertView {
	NSLog(@"AlertView Will Dismiss: '%@'", alertView.titleLabel.text);
}

- (void)alertViewDidDismiss:(AMSmoothAlertView *)alertView {
	NSLog(@"AlertView Did Dismiss: '%@'", alertView.titleLabel.text);
}

@end
