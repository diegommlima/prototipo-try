//
//  PQAppDelegate.h
//  PrototypeAnimation
//
//  Created by Diego Lima on 23/09/14.
//  Copyright (c) 2014 Diego Lima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PQAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
