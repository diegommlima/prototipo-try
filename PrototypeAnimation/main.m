//
//  main.m
//  PrototypeAnimation
//
//  Created by Diego Lima on 23/09/14.
//  Copyright (c) 2014 Diego Lima. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PQAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PQAppDelegate class]));
    }
}
