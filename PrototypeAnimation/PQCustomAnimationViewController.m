//
//  PQCustomAnimationViewController.m
//  PrototypeAnimation
//
//  Created by Diego Lima on 24/09/14.
//  Copyright (c) 2014 Diego Lima. All rights reserved.
//

#import "PQCustomAnimationViewController.h"

static CGFloat kButtonsVelocityRating = 0.9f;

@interface PQCustomAnimationViewController ()

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, weak) UIButton *printButton;
@property (nonatomic, weak) UIButton *mailButton;
@property (nonatomic, weak) UIButton *facebookButton;

@property (nonatomic, strong) UIDynamicAnimator *animator;

@end

@implementation PQCustomAnimationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Animação Customizada";

    self.imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"foto"]];
    self.imageView.frame = CGRectMake(40.0f, -200.0f, 240.0f, 200.0f);
    [self.view addSubview:self.imageView];

    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];

    self.printButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.printButton setBackgroundImage:[UIImage imageNamed:@"printer_icon.png"] forState:UIControlStateNormal];
    self.printButton.adjustsImageWhenHighlighted = NO;
    [self.view addSubview:self.printButton];
    
    self.mailButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.mailButton setBackgroundImage:[UIImage imageNamed:@"mail_icon.png"] forState:UIControlStateNormal];
    self.mailButton.adjustsImageWhenHighlighted = NO;
    [self.view addSubview:self.mailButton];
    
    self.facebookButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.facebookButton setBackgroundImage:[UIImage imageNamed:@"fb_icon.png"] forState:UIControlStateNormal];
    self.facebookButton.adjustsImageWhenHighlighted = NO;
    [self.view addSubview:self.facebookButton];
    
    self.printButton.center = CGPointMake(75.0f, 280.0f);
    self.mailButton.center = CGPointMake(self.view.frame.size.width/2, 270.0f);
    self.facebookButton.center = CGPointMake(self.view.frame.size.width-65.0f, 270.0f);

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [self showGravityAnimated];
    [self showAnimatingButtons];
}

- (void)showGravityAnimated {
    
    [self.animator removeAllBehaviors];

    UIGravityBehavior *gravityBehavior = [[UIGravityBehavior alloc] initWithItems:@[self.imageView]];
    gravityBehavior.gravityDirection = CGVectorMake(0, 3);//Velocidade
    [self.animator addBehavior:gravityBehavior];

    UICollisionBehavior *collisionBehavior = [[UICollisionBehavior alloc] initWithItems:@[self.imageView]];
    [collisionBehavior addBoundaryWithIdentifier:@"imageBoundary"
                                       fromPoint:CGPointMake(CGRectGetMinX(self.imageView.frame), 220.0f)
                                         toPoint:CGPointMake(CGRectGetMaxX(self.imageView.frame), 220.0f)];
    [self.animator addBehavior:collisionBehavior];

//    UIPushBehavior *pushBehavior = [[UIPushBehavior alloc] initWithItems:@[self.imageView]
//                                                                    mode:UIPushBehaviorModeInstantaneous];
//    pushBehavior.magnitude = 4;
//    [self.animator addBehavior:pushBehavior];
    
    UIDynamicItemBehavior *menuViewBehavior = [[UIDynamicItemBehavior alloc] initWithItems:@[self.imageView]];
    menuViewBehavior.elasticity = 0.4;//Elasticidade
    [self.animator addBehavior:menuViewBehavior];
}

- (void)showAnimatingButtons {

    
    [UIView animateWithDuration:0.3*kButtonsVelocityRating
                          delay:0.7f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.printButton.frame = CGRectMake(40.0f, 245.0f, 70.0f, 70.0f);

                     } completion:nil];
    
    [UIView animateWithDuration:0.3*kButtonsVelocityRating
                          delay:0.8f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.mailButton.frame = CGRectMake((self.view.frame.size.width- 70.0f)/2, 245.0f, 70.0f, 70.0f);
                         
                     } completion:nil];
    
    [UIView animateWithDuration:0.3*kButtonsVelocityRating
                          delay:0.9f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.facebookButton.frame = CGRectMake(self.view.frame.size.width - 40.0f - 70.0f, 245.0f, 70.0f, 70.0f);
                         
                     } completion:nil];
}

@end
